# Base

Install a new "base" on a Linux host. The host can be a server, virtual machine, or container.

This installs my [dotfiles](https://gitlab.com/milohax-net/radix/dotfiles), and shell libraries [bashing](https://gitlab.com/milohax-net/radix/bashing) and [fishing](https://gitlab.com/milohax-net/radix/fishing), and enough utilities to make them functional.

It's separate to [bootstrap](https://gitlab.com/milohax-net/radix/bootstrap) because these plays are intended to be the base of operations for any host, not only workstations.
